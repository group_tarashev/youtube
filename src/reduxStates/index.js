import {configureStore, createSlice} from '@reduxjs/toolkit'

const initialState = {
    mode: localStorage.getItem('Theme', 'light') || 'dark',
    menu: false,
    login: false,
    srchData: [],
}

export const modeSlice = createSlice({
    name:'mode',
    initialState,
    reducers:{
        setMode: (state) =>{
            state.mode = state.mode === "light" ? "dark" : 'light'
        },
        setMenu: (state) =>{
            state.menu = state.menu === false ? true : false
        },
        setLogin: (state) =>{
            state.login = state.login === false ? true : false
        },     
        setSrchData: (state, action) =>{
            return {
                ...state,
                srchData: action.payload
            }
        }
    }
});



export const {setMode, setMenu, setLogin, setSrchData} = modeSlice.actions;
export const store = configureStore({
    reducer: modeSlice.reducer
}) 