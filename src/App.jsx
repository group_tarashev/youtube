import { useSelector } from "react-redux";
import {
  Box,
  createTheme,
  ThemeProvider,
} from "@mui/material";
import { themeSetting } from "./theme";
import {useMemo} from "react";
import Navbar from "./components/navbar/Navbar";
import Sidebar from "./components/sidebar/Sidebar";
import Category from "./components/category/Category";
import Content from "./components/Content/Content";
import MobileBottomNav from "./components/custom/MobileBottomNav";
import Info from "./components/custom/Info";


function App() {
  const mode = useSelector((state) => state.mode);
  const theme = useMemo(() => createTheme(themeSetting(mode)), [mode]);

  
  return (
      <ThemeProvider theme={theme}>
        <Box>
          <Info/>
          <Navbar />
          <Box
            sx={{
              display: "flex",
              justifyContent:'space-between',
              backgroundColor: theme.palette.background.alt,
            }}
          >
            <Sidebar />
            <Box sx={{ display: "flex", flexDirection: "column" }}>
              <Category />
              <Content/>
              {/* <Seetheme /> */}
              <MobileBottomNav/>
            </Box>
          </Box>
        </Box>
      </ThemeProvider>
  );
}

export default App;
