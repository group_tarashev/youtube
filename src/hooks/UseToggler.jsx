import  { useState } from "react";

const UseToggler = () => {
  const [el, setEl] = useState(null);
  const open = Boolean(el);

  const handleClose = (e) => {
    setEl(null);
  };

  const handleOpen = (e) => {
    setEl(e.currentTarget);
  };
  return {el, open, handleClose, handleOpen}
};

export default UseToggler;
