import { Box, Divider, Menu, Tooltip, Typography } from '@mui/material'
import React from 'react'
import UseToggler from '../../hooks/UseToggler'
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import { CustomBox, CustomMenuItem } from '../custom/Setting';
import Settings from '@mui/icons-material/Settings';
const Notification = () => {
    const {el, open, handleClose, handleOpen} = UseToggler();
  return (
    <Box>

    <Tooltip title="Notification" onClick={handleOpen} id="basic-button">
        <NotificationsNoneOutlinedIcon sx={{cursor:'pointer'}}/>
      </Tooltip>
      <Menu
        id="basic-setting-menu"
        open={open}
        onClose={handleClose}
        anchorEl={el}
        MenuListProps={{
          "aria-labelledby": "basic-setting-menu",
        }}
      >
        <Box
            sx={{width:300, height:350}}
        >
            <CustomMenuItem>
                <CustomBox>
                    <Typography variant='h7'>Notification</Typography>
                    <Settings/>
                </CustomBox>
            </CustomMenuItem>
            <Divider/>
        </Box>
        </Menu>
    </Box>

  )
}

export default Notification