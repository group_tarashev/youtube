import React from "react";
import VideoCallOutlinedIcon from "@mui/icons-material/VideoCallOutlined";
import { Box, Menu, Tooltip, Typography } from "@mui/material";
import UseToggler from "../../hooks/UseToggler";
import { CustomBox, CustomMenuItem } from "../custom/Setting";
import SlideshowOutlinedIcon from "@mui/icons-material/SlideshowOutlined";
import SensorsOutlinedIcon from "@mui/icons-material/SensorsOutlined";

const UploadVideo = () => {
  const { el, open, handleOpen, handleClose } = UseToggler();
  return (
    <Box>
      <Tooltip title="Upload Video" onClick={handleOpen}>
        <VideoCallOutlinedIcon sx={{cursor:'pointer'}} />
      </Tooltip>
      <Menu
        id="basic-notification-menu"
        open={open}
        anchorEl={el}
        onClose={handleClose}
        MenuListProps={{
          "aria-lebelledby": "basic-notification-menu",
        }}
      >
        <Box sx={{ display: "flex", flexDirection: "column" }}>
          <CustomMenuItem>
            <CustomBox>
              <Box sx={{display:'flex', gap:'10px'}}>
                <SlideshowOutlinedIcon />
                <Typography>Upload Video</Typography>
              </Box>
            </CustomBox>
          </CustomMenuItem>
          <CustomMenuItem>
            <CustomBox>
              <Box sx={{display:'flex', gap:'10px'}}>
                <SensorsOutlinedIcon />
                <Typography>Go Live</Typography>
              </Box>
            </CustomBox>
          </CustomMenuItem>
        </Box>
      </Menu>
    </Box>
  );
};

export default UploadVideo;
