import { Button, useTheme, styled } from "@mui/material";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import { setLogin } from "../../reduxStates/index";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import useMediaQuery from "@mui/material/useMediaQuery";
const StlButton = styled(Button)(({ theme }) => ({
  [theme.breakpoints.down("md")]: {
    fontSize: "12px",
    textTransform: "lowercase",
    width: "max-content",
  },
  [theme.breakpoints.down("sm")]: {
    padding: "0px",
    border: "none",
    width: "0px",
    marginRight:'10px',
    display:'flex'
  },
  textTransform:'capitalize',
  fontSize: "15px",
  borderRadius: "20px",
  border: "1px solid lightgrey",
  padding: "5px 10px",
  display: "flex",
  alignItems: "center",
  justifyContent:'flex-start',
  gap: "5px",
  outline: "none",
  cursor: "pointer",
}));

const SignButton = () => {
  const matches = useMediaQuery((theme) => theme.breakpoints.down("sm"));

  const { palette } = useTheme();
  const login = useSelector((state) => state.login);
  const dispatch = useDispatch();

  return (
    <StlButton
      onClick={() => {
        dispatch(setLogin(!login));
      }}
      sx={{
        color: palette.primary.main,
      }}
    >
      <AccountCircleOutlinedIcon /> {matches || <span>Sign in</span>}
    </StlButton>
  );
};

export default SignButton;
