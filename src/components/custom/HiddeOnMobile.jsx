import { Box, styled } from '@mui/material'
import React from 'react'

export const HiddeOnMobile = styled(Box)(({theme}) =>({
    [theme.breakpoints.down('sm')]:{
        display: 'none'
    }
}))
const HiddeMobile = () => {
  return (
    <div>HiddeOnMobile</div>
  )
}

export default HiddeMobile