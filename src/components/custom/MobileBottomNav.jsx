import { Box, IconButton, styled, Typography, useTheme } from '@mui/material'
import React from 'react'
import HomeIcon from "@mui/icons-material/Home";
import PlayCircleOutlineOutlinedIcon from '@mui/icons-material/PlayCircleOutlineOutlined';
import SubscriptionsOutlinedIcon from '@mui/icons-material/SubscriptionsOutlined';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import LibraryBooksIcon from '@mui/icons-material/LibraryBooks';

const BottomBar = styled(Box)(({theme}) =>({
    [theme.breakpoints.up('sm')]:{
        display: 'none'
    },
    position:'sticky',
    bottom: -2,
    left: 0,
    widht:'100vw',
    height: '70px',
    display:'flex',
    alignItems:'center',
    justifyContent: 'center',
    fontSize:'15px',
    borderTop: '1px solid lightgrey'
}))
const StIconButton = styled(IconButton)(({theme}) =>({
    display:'flex',
    alignItems:'center',
    justifiContent: 'center',
    gap:'3px',
    flexDirection:'column',
    width:'70px'
}))
const StTypo = styled(Typography)({
  fontSize: '12px',
})

const MobileBottomNav = () => {
  const {palette} = useTheme();
  return (
    <BottomBar sx={{backgroundColor:palette.background.default}}>
        <StIconButton>
            <HomeIcon/>
            <StTypo>Home</StTypo>
        </StIconButton>
        <StIconButton>
            <PlayCircleOutlineOutlinedIcon/>
            <StTypo>Shorts</StTypo>
        </StIconButton>
        <StIconButton>
            <AddCircleOutlineIcon sx={{fontSize:50}}/>
        </StIconButton>
        <StIconButton>
            <SubscriptionsOutlinedIcon/>
            <StTypo>Subsriptions</StTypo>
        </StIconButton>
        <StIconButton>
            <LibraryBooksIcon/>
            <StTypo>Library</StTypo>
        </StIconButton>
    </BottomBar>
  )
}

export default MobileBottomNav