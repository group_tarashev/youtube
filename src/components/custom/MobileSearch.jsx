import Search from "@mui/icons-material/Search";
import {
  Box,
  Button,
  Drawer,
  IconButton,
  InputBase,
  MenuItem,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import React, { useState } from "react";
import UseToggler from "../../hooks/UseToggler";
import { fetchYoutube } from "../../axios/axiosApi";
import { useDispatch } from "react-redux";
import { setSrchData } from "../../reduxStates";

const MobileSearch = () => {
  const { el, open, handleClose, handleOpen } = UseToggler();
  const [search, setSearch] = useState(null);
  const dispatch = useDispatch();

  const handleSearch = (e) => {
    e.preventDefault();
    fetchYoutube(search).then((res) => {
      dispatch(setSrchData(res.data.items));
    });
  };
  return (
    <Box>
      <Button>
        <IconButton onClick={handleOpen}>
          <Search />
        </IconButton>
      </Button>
      <Drawer
        anchorEl="left"
        open={open}
        sx={{ flexShrink: 0, widht: "100%" }}
        variant="persistent"
      >
        <MenuItem
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <InputBase
            id="search-input"
            placeholder="Search"
            sx={{
              flex: 2,
              outlineColor: "lightgrey",
              borderRadius: "20px",
              border: "1px solid lightgrey",
              pl: 2,
            }}
            onChange={(e) => setSearch(e.target.value)}
          />
          <IconButton
            sx={{ flex: 1, display: "flex", alignItems: "center", gap: 5 }}
            onClick={handleClose}
          >
            <Search onClick={handleSearch} />
            <CloseIcon onClick={handleClose} />
          </IconButton>
        </MenuItem>
      </Drawer>
    </Box>
  );
};

export default MobileSearch;
