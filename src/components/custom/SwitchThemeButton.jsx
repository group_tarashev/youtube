import DarkMode from "@mui/icons-material/DarkMode";
import WbSunny from "@mui/icons-material/WbSunny";
import { FormControlLabel, FormGroup, Switch } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { CustomBox } from "./Setting";
import { setMode } from "../../reduxStates";

const SwitchThemeButton = () => {
  const dispatch = useDispatch();
  const mode = useSelector((state) => state.mode);
  const [checked, setChecked] = useState(mode === "light");

  useEffect(() => {
    localStorage.setItem("Theme", mode);
  }, [mode]);

  return (
    <FormGroup>
      <CustomBox>
        <DarkMode />
        <FormControlLabel
          checked={checked}
          onClick={() => {
            dispatch(setMode(mode));
            setChecked(!checked);
          }}
          control={<Switch />}
        />
        <WbSunny sx={{ marginLeft: "-16px" }} />
      </CustomBox>
    </FormGroup>
  );
};

export default SwitchThemeButton;
