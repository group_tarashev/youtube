import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import MoreVert from "@mui/icons-material/MoreVert";
import {
  Box,
  Divider,
  Menu,
  Tooltip,
  styled,
  MenuItem,
  Avatar,
} from "@mui/material";
import React from "react";
import SwitchThemeButton from "./SwitchThemeButton";
import { settingMenu, logSettMenu } from "../../data/dataMenu";
import UseToggler from "../../hooks/UseToggler";
import { useDispatch, useSelector } from "react-redux";
import { setLogin } from "../../reduxStates/index";
export const CustomMenuItem = styled(MenuItem)({
  display: "flex",
  alignItems: "center",
  gap: "20px",
  padding: " 5px 10px",
  justifyContent: "space-between",
});

export const CustomBox = styled(Box)({
  width: "100%",
  display: "flex",
  alignItems: "center",
  justifyContent: "space-between",
  gap: "20px",
});

const StAvatar = styled(Avatar)({
  width: "25px",
  height: "25px",
  padding: "15px",
  borderRadius: "50%",
  border: "none",
  backgroundColor: "red",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  cursor: "pointer",
});


const Setting = () => {

  const { el, open, handleClose, handleOpen } = UseToggler();
  const login = useSelector((state) => state.login);
  const dispatch = useDispatch();
  return (
    <Box>
      <Tooltip title="Setting" onClick={handleOpen} id="basic-button">
        {login ? (
          <StAvatar>
            I
          </StAvatar>
        ) : (
          <MoreVert sx={{ cursor: "pointer" }} />
        )}
      </Tooltip>
      <Menu
        id="basic-setting-menu"
        // elevation={0}
        open={open}
        onClose={handleClose}
        anchorEl={el}
        MenuListProps={{
          "aria-labelledby": "basic-setting-menu",
        }}
      >
        {
          ///// Wen user is log in there more items
          login
            ? logSettMenu.map((item, i) => (
                <React.StrictMode key={i}>
                  {item.divider ? (
                    <Divider />
                  ) : (
                    <CustomMenuItem>
                      <CustomBox>
                        <Box
                          sx={{
                            display: "flex",
                            gap: "10px",
                            alignItems: "center",
                          }}
                          onClick={() => {
                            i === 6 && dispatch(setLogin(false));
                            handleClose();
                          }}
                        >
                          {item.userIcon}
                          {item.userText}
                        </Box>
                        {item.arrow && <KeyboardArrowRight />}
                      </CustomBox>
                    </CustomMenuItem>
                  )}
                </React.StrictMode>
              ))
            : null
        }
        {/* when user is not log in */}
        {settingMenu.map((item, i) => (
          <React.StrictMode key={i}>
            {item.divider ? (
              <Divider />
            ) : item.id === 3 ? (
              <CustomMenuItem>
                <SwitchThemeButton />
              </CustomMenuItem>
            ) : (
              <CustomMenuItem>
                <CustomBox>
                  <Box
                    sx={{ display: "flex", gap: "10px", alignItems: "center" }}
                  >
                    {item.icon}
                    {item.text}
                  </Box>
                  {item.arrow && <KeyboardArrowRight />}
                </CustomBox>
              </CustomMenuItem>
            )}
          </React.StrictMode>
        ))}
      </Menu>
    </Box>
  );
};

export default Setting;
