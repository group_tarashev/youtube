import {
  Box,
  Button,
  List,
  ListItem,
  Modal,
  styled,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React, { useEffect, useState } from "react";

const MyModal = styled(Modal)(({ theme }) => ({
  backgroundColor: "white",
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "max-content",
  height: "max-content",
  border: "none",
  outline: "none",
  borderWidth: "none",
  [theme.breakpoints.down("md")]: {
    display: "none",
  },
  ":hover":{
      backgroundColor:'grey',
  }
}));

const Info = () => {
  const [open, setOpen] = useState(false);
  const matches = useMediaQuery((theme) => theme.breakpoints.down("md"));
  useEffect(() => {
    return () => {
      matches || setOpen(true);
    };
  }, []);

  matches ||
    (window.onload = () => {
      setOpen(true);
    });
  const handleClose = () => {
    setOpen(false);
  };
  console.log(open);
  return (
    <div>
      <MyModal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-titl"
        aria-describedby="modal-modal-descriptio"
        hideBackdrop
      >
        <Box p={2}>
          <Button
            sx={{
              color: "black",
              border:'1px solid black',
              ":hover": { backgroundColor: "whitesmoke", color: "black" },
            }}
            onClick={handleClose}
          >
            Close X
          </Button>
          <Typography variant="h5">This is demo-copy of YouTube</Typography>
          <Typography variant="h6">
            API is provided by RapidAPI, The site is build with tools:
          </Typography>
          <List>
            <ListItem>
              <Typography variant="h7">Javascript</Typography>
            </ListItem>
            <ListItem>
              <Typography variant="h7">React: Redux states</Typography>
            </ListItem>
            <ListItem>
              <Typography variant="h7">Material Ui</Typography>
            </ListItem>
          </List>
          <Typography variant="h6" sx={{ width: 400 }}>
            Data is dynamic fetched via local json file. You can search vidoes
            to 50 items, provided API by RapidAPI. When you click on image of
            video will send you to YouTube.com
          </Typography>
        </Box>
      </MyModal>
    </div>
  );
};

export default Info;
