import { Box, styled, Tab, Tabs, useTheme } from "@mui/material";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchYoutube } from "../../axios/axiosApi";
import { tabsMenu } from "../../data/dataTabs";
import { setSrchData } from "../../reduxStates";

const CatBox = styled(Box)(({theme}) =>({
  [theme.breakpoints.down('sm')]:{
    display:'none'
  }
}))

const Category = () => {
  const menu = useSelector((state) => state.menu);
  const [value, setValue] = useState(0);
  const { palette } = useTheme();
  const dispatch = useDispatch();

  const tabChange = (e, newValue) => {
    setValue(newValue);
    if (e.target.innerText === "All") {
      fetchYoutube("").then((res) => {
        dispatch(setSrchData(res.data.items));
      });
    } else {
      fetchYoutube(e.target.innerText).then((res) => {
        dispatch(setSrchData(res.data.items));
      });
    }
  };

  return (
    <CatBox
      width={menu ? "calc(100vw - 300px)" : "calc(100vw - 70px)"}
      zIndex={20}
      sx={{
        display: "flex",
        height: "70px",
        position: "sticky",
        top: "64px",
        backgroundColor: palette.background.default,
        color: palette.neutral.dark,
      }}
    >
      <Tabs
        value={value}
        onChange={tabChange}
        variant="scrollable"
        aria-label="scrollable tabs"
        scrollButtons={true}
        visibleScrollbar={false}
        sx={{ display: "flex", alignItems: "center" }}
      >
        {tabsMenu.map((item, i) => (
          <Tab key={i} label={item} />
        ))}
      </Tabs>
    </CatBox>
  );
};

export default Category;
