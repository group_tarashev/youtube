import { useTheme } from "@emotion/react";
import {
  Box,
  Divider,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  styled,
  Tooltip,
  Typography,
} from "@mui/material";

import React from "react";
import SignButton from "../custom/SignButton";
import { useSelector } from "react-redux";
import { sideBarItems } from "../../data/dataMenu";

const toolTip = ["Home", "Short", "Subscriptions", "History"];

const SideBox = styled(Box)(({theme}) =>({
  [theme.breakpoints.down('sm')]:{
    display:'none'
  },
}))

const Sidebar = () => {
  const { palette } = useTheme();
  const menu = useSelector((state) => state.menu);
  const login = useSelector((state) => state.login);

  const listItem = (item) => (
    <ListItem>
      <ListItemButton sx={{ m: 0, p: 0 }}>
        <ListItemIcon>{item.icon}</ListItemIcon>
        <ListItemText>{menu && item.text}</ListItemText>
      </ListItemButton>
    </ListItem>
  );

  return (
    <SideBox
      minWidth={menu ? "300px" : "70px"}
      sx={{
        height: "calc(100vh - 83px)",
        backgroundColor: palette.background.default,
        color: palette.neutral.dark,
        position: "sticky",
        padding: "20px 0px",
        top: "64px",
        overflowX: "scroll",
        "&::-webkit-scrollbar": {
          width: "10px",
          display: "none",
        },
        "&::-webkit-scrollbar:hover": {
          width: "10px",
        },
        "::-webkit-scrollbar-track": {
          background: palette.background.alt,
        },
        "::-webkit-scrollbar-thumb": {
          background: palette.neutral.mediumMain,
        },
      }}
    >
      <List>
        {sideBarItems.map((item, i) => (
          <React.StrictMode key={i}>
            {item.divider ? (
              menu && <Divider />
            ) : menu && item.title ? (
              <Box sx={{ p: 1 }}>
                <Typography variant="h6" textTransform={"uppercase"}>
                  {item.text}
                </Typography>
              </Box>
            ) : menu && !login && item.button ? (
              <Box sx={{ mt: 2, mb: 2, alignItems: "center" }}>
                <SignButton />
                {!login && (
                  <Typography sx={{ pt: 1 }}>{item.textLog}</Typography>
                )}
              </Box>
            ) :  menu ? (
              listItem(item)
            ) : (
              i < 4 && (
                <Tooltip title={toolTip[i]} placement="right-start" arrow>
                  {listItem(item, menu)}
                </Tooltip>
              )
            )}
          </React.StrictMode>
        ))}
      </List>
    </SideBox>
  );
};

export default Sidebar;
