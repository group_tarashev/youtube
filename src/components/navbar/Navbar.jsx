import React, { useState } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import {
  useTheme,
  InputBase,
  Tooltip,
  Button,
  styled,
  Typography,
  useMediaQuery,
} from "@mui/material";
import SignButton from "../custom/SignButton";
import { useDispatch, useSelector } from "react-redux";
import { modeSlice, setMenu, setSrchData } from "../../reduxStates";
import SearchIcon from "@mui/icons-material/Search";
import logo from "../../img/YouTube-Icon-Full-Color-Logo.wine.svg";
import Setting from "../custom/Setting";
import Notification from "../notification/Notification";
import MicOutlinedIcon from '@mui/icons-material/MicOutlined';
import UploadVideo from "../uploadVideo/UploadVideo";
import { fetchYoutube } from "../../axios/axiosApi";
import MainData from "../../axios/mainData";
import { HiddeOnMobile } from "../custom/HiddeOnMobile";
import MobileSearch from "../custom/MobileSearch";

const StIconButton = styled(IconButton)(({theme}) =>({
  [theme.breakpoints.down('sm')]:{
    display:'none'
  }
}))

export default function Navbar() {
  const matches = useMediaQuery((theme) => theme.breakpoints.down('sm'))

  const menu = useSelector((state) => state.menu);
  const login = useSelector((state) => state.login);

  const dispatch = useDispatch();
  const { palette } = useTheme();

  const [search, setSearch] = useState('');

  const handleChange = (e) =>{
    setSearch(e.target.value)
  }
  const handleSearch = (e) =>{
    e.preventDefault();
      fetchYoutube(search).then((res) =>{
        dispatch(setSrchData(res.data.items))
      });
      
  }

  
  return (
    <Box sx={{ flexGrow: 1, position: "fixed", top: 0, zIndex: 20, width:'100vw',zIndex: 10 }}>
      <AppBar
        position="static"
        sx={{ backgroundColor: palette.background.default, boxShadow: "none" }}
      >
        <Toolbar
          sx={{display: 'flex', justifyContent:'space-between', alignItems:'center'}}
        >
          
          <StIconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={() => dispatch(setMenu(!menu))}
          >
            <MenuIcon />
          </StIconButton>
          <Box sx={{ flexGrow: 1, display: "flex", alignItems: "center", cursor:'pointer', mr:2 }} onClick={() => {window.location.reload(); MainData()}}>
            <img src={logo} alt="" style={{ width: "50px" }} />
            <Typography variant="h6" fontWeight={500}>YouTube</Typography>
          </Box>
          {matches && <MobileSearch/>}
          <HiddeOnMobile sx={{ flexGrow: 5, display: "flex", alignItems: "center", cursor:'pointer'}}>
            <InputBase
              id='search-input'
              sx={{
                border: "1px solid lightgrey",
                width: "80%",
                borderRadius: "40px 0 0 40px",
                padding: "0 20px",
              }}
              onChange={handleChange}
              placeholder="Search"
            />
            <Tooltip title="Search">
              <Button
              
                sx={{
                  border: "1px solid lightgrey",
                  borderRadius: "0 40px 40px 0",
                  borderLeft: "none",
                  padding: "4px",
                  backgroundColor: palette.background.alt,
                  marginRight:'10px',
                }}
                onClick={handleSearch} 
              >
                <SearchIcon sx={{ color: palette.neutral.dark }}/>
              </Button>
            </Tooltip>
            <Tooltip title='Search with your voice'>
                <MicOutlinedIcon sx={{cursor:'pointer'}}/>
            </Tooltip>
          </HiddeOnMobile>
          


          <Box
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "center",
              gap: '5px',
              ml:2,
              mr:2
            }}
          >
            <UploadVideo/>
            <Notification/>
            <Setting/>  
            {login ? null :<SignButton />}
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
