import { Box, styled } from '@mui/material'
import React from 'react'
import { useSelector } from 'react-redux'
import SingleCard from './SingleCard'

const Content = () => {

  const menu = useSelector(state => state.menu)
  return (
    <Box
      width={menu ? 'calc(100vw - 360)' : 'calc(100vw - 120)'}
      sx={{marginTop:'90px', display:'flex', alignItems:'center', justifyContent:'center' }}
    >
      <SingleCard/>
    </Box>
  )
}

export default Content