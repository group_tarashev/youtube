import MoreVert from "@mui/icons-material/MoreVert";
import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  IconButton,
  Tooltip,
  Grid,
  Typography,
  useTheme,
  Link,
} from "@mui/material";
import React from "react";
import moment from "moment";
import { useSelector } from "react-redux";
import MainData from "../../axios/mainData";

const SingleCard = () => {
  const { palette } = useTheme();
  const srchData = useSelector((state) => state.srchData);
  // const  data = useSelector((state) => state)
  // console.log(data);
    MainData(); /// calll main data

  return (
    <Grid
      container
      rowSpacing={1}
      columnSpacing={{ xs: 2, sm: 4 }}
      gap={4}
      alignContent="center"
      justifyContent="center"
    >
      {srchData?.map((item, i) => {
        const { videoId } = item.id;
        const { title, publishTime } = item.snippet;
        const { url } = item.snippet.thumbnails.medium;

        return (
          <Card
            key={i}
            sx={{
              boxShadow: 0,
              maxWidth: 300,
              minWidth: 300,
              borderRadius: "20px",
              backgroundColor: palette.background.default,
              transition: "all 0.3s ease",
              cursor: "pointer",

              "&:hover": {
                transform: "scale(1.2)",
                zIndex: 4,
              },
            }}
          >
            <Link href={`https://www.youtube.com/watch?v=${videoId}`}>
              <CardMedia
                component="img"
                height="194"
                image={url}
                alt="Paella dish"
              />
            </Link>
            <CardContent sx={{ display: "flex", gap: 2 }}>
              <CardHeader
                sx={{ padding: 0 }}
                avatar={
                  <Avatar aria-label="recipe">
                    <Tooltip title={item.creator}>
                      <img src={url} alt="" />
                    </Tooltip>
                  </Avatar>
                }
                subheader={
                  <Typography variant="span" sx={{ fontSize: "12px" }}>
                    {title}
                  </Typography>
                }
              />

              <IconButton>
                <MoreVert />
              </IconButton>
            </CardContent>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
                paddingBottom: "20px",
                paddingLeft: "20px",
              }}
            >
              <Typography variant="h7" sx={{ p: 0 }}>
                Posted
              </Typography>
              <Typography component="span" sx={{ p: 0, fontSize: "12px" }}>
                {moment(publishTime).fromNow()}
              </Typography>
            </Box>
          </Card>
        );
      })}
    </Grid>
  );
};

export default SingleCard;
