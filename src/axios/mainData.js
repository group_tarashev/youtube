import { useEffect } from "react"
import { useDispatch } from "react-redux"
import {setSrchData} from '../reduxStates/index'
import { fetchYoutube } from "./axiosApi";
const MainData = () =>{
    const dispatch = useDispatch();
    useEffect(()=>{

        fetchYoutube("All").then(res => dispatch(setSrchData(res.data.items))).catch(err => console.log(err))
    },[])
    }   

export default MainData