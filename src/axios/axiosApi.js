import axios from "axios";


const options = {
  method: "GET",
  url: "https://youtube-v31.p.rapidapi.com/search",
  params: {
    q: "music",
    part: "snippet,id",
    regionCode: "US",
    maxResults: "100",
    popularity: "popularity",
  },
  headers: {
    "X-RapidAPI-Key": "3c1f4541dcmsh1a386c128705e41p1fab5bjsnaf7c0d571e86",
    "X-RapidAPI-Host": "youtube-v31.p.rapidapi.com",
  },
};

export const fetchYoutube = (searchQuery) => {
  const ApiOptions = {
    ...options,
    params: {
      ...options.params,
      q: searchQuery,
    },
  };
  return axios.request(ApiOptions)
};
