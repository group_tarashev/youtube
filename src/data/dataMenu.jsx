import HomeIcon from "@mui/icons-material/Home";
import PlayCircleOutlineIcon from "@mui/icons-material/PlayCircleOutline";
import SubscriptionsIcon from "@mui/icons-material/Subscriptions";
import React from "react";
import SignButton from "../components/custom/SignButton";
import WhatshotIcon from "@mui/icons-material/Whatshot";
import MusicNoteIcon from "@mui/icons-material/MusicNote";
import SportsEsportsIcon from "@mui/icons-material/SportsEsports";
import EmojiEventsIcon from "@mui/icons-material/EmojiEvents";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import youtubePL from "../img/ytMU.png";
import youtubePR from "../img/ytPR.svg";
import youtubeKD from "../img/ytKD.svg";
import youtubeTV from "../img/ytTV.png";
import SettingsIcon from "@mui/icons-material/Settings";
import EmojiFlagsIcon from "@mui/icons-material/EmojiFlags";
import QuestionMarkIcon from "@mui/icons-material/QuestionMark";
import PriorityHighIcon from "@mui/icons-material/PriorityHigh";
import HistoryIcon from "@mui/icons-material/History";
import { Divider } from "@mui/material";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import TranslateIcon from "@mui/icons-material/Translate";
import SecurityIcon from "@mui/icons-material/Security";
import LanguageIcon from "@mui/icons-material/Language";
import KeyboardIcon from "@mui/icons-material/Keyboard";
import NotStartedOutlinedIcon from '@mui/icons-material/NotStartedOutlined';
import GroupOutlinedIcon from '@mui/icons-material/GroupOutlined';
import LoginOutlinedIcon from '@mui/icons-material/LoginOutlined';
import LocalAtmOutlinedIcon from '@mui/icons-material/LocalAtmOutlined';
// /////// sidebar menu item
export const sideBarItems = [
  {
    icon: <HomeIcon />,
    text: "Home",
  },
  {
    icon: <PlayCircleOutlineIcon />,
    text: "Short",
  },
  {
    icon: <SubscriptionsIcon />,
    text: "Subscriptions",
  },
  {
    icon: <HistoryIcon />,
    text: "History",
  },
  { divider: true, divide: <Divider /> },
  {
    button: <SignButton />,
    textLog: "Sign in to like videos, comment, and subscribe.",
  },
  { divider: true, divide: <Divider /> },
  {
    title: true,
    text: "Explore",
  },
  {
    icon: <WhatshotIcon />,
    text: "Trending",
  },
  {
    icon: <MusicNoteIcon />,
    text: "Music",
  },
  {
    icon: <SportsEsportsIcon />,
    text: "Gaming",
  },
  {
    icon: <EmojiEventsIcon />,
    text: "Sport",
  },
  { divider: true, divide: <Divider /> },
  {
    icon: <AddCircleOutlineIcon />,
    text: "Browser More",
  },
  { divider: true, divide: <Divider /> },
  {
    title: true,
    text: "More from YouTube",
  },
  {
    icon: (
      <img
        src={youtubePR}
        alt=""
        style={{ width: "32px", height: "32px", objectFit: "cover" }}
      />
    ),
    text: "YouTube Premium",
  },
  {
    icon: (
      <img
        src={youtubePL}
        alt=""
        style={{ width: "32px", height: "32px", objectFit: "cover" }}
      />
    ),
    text: "YouTube Music",
  },
  {
    icon: (
      <img
        src={youtubeKD}
        alt=""
        style={{ width: "32px", height: "32px", objectFit: "cover" }}
      />
    ),
    text: "YouTube Kids",
  },
  {
    icon: (
      <img
        src={youtubeTV}
        alt=""
        style={{ width: "32px", height: "32px", objectFit: "cover" }}
      />
    ),
    text: "YouTube TV",
  },
  { divider: true, divide: <Divider /> },
  {
    icon: <SettingsIcon />,
    text: "Setting",
  },
  {
    icon: <EmojiFlagsIcon />,
    text: "Report History",
  },
  {
    icon: <QuestionMarkIcon />,
    text: "Help",
  },
  {
    icon: <PriorityHighIcon />,
    text: "Send feedback",
  },
  {
    list: [
      "About",
      "Press",
      "Copyright",
      "Contact Us",
      "Creators",
      "Developers",
    ],
  },
];

////Navbar menu Setting


export const logSettMenu = [
  {
    id:12412,
    userIcon: <div style={{width:"30px", height:'30px' ,padding:'20px', borderRadius:'50%', border:'none', backgroundColor:'red', display:'flex', alignItems:'center', justifyContent:'center'}} >I</div>,
    userText: 'User Username',
    arrow: false
  },
  {
    id:12415,
    userText: 'Manage You Account',
    arrow: true
  },
  {
    id: 215512,
    divider: true
  },
  {
    id:61612,
    userIcon: <AccountCircleIcon/>,
    userText: 'Your channel',
    arrow: false,
  },
  {
    id: 61235,
    userIcon: <NotStartedOutlinedIcon/>,
    userText:"YouTube Studio",
    arrow: false,
  },
  {
    id: 612312,
    userIcon:<GroupOutlinedIcon/>,
    userText:"Switch Acount",
    arrow: true,
  },
  {
    id: 6112,
    userIcon:<LoginOutlinedIcon/>,
    userText:"Sign out",
    arrow: false,
  },
  {
    id:162335,
    divider: true,
  },
  {
    id: 6112132,
    userIcon:<LocalAtmOutlinedIcon/>,
    userText:"Purches and Memberships",
    arrow: false,
  },

]


export const settingMenu = [
  
  {
    id:1,
    icon: <AccountCircleIcon />,
    text: 'You data in YouTube'
  },
  {
    id:2,
    divider: true,
  },
  {
    id:3,
    icon: <TranslateIcon />,
    text: 'Language',
    arrow: true

  },
  {
    id:4,
    icon: <SecurityIcon />,
    text: 'Restricted mode: Off',
    arrow: true

  },
  {
    id:5,
    icon: <LanguageIcon />,
    text: 'Location: Bulgaria',
    arrow: true

  },
  {
    id:6,
    icon: <KeyboardIcon />,
    text: 'Keybord Shortcuts',
    arrow: true

  },
  {
    id:7,divider: true},
  {
    id:8,
    icon: <SettingsIcon />,
    text: 'Setting',
    arrow: true

  },
  {
    id:9,
    icon: <QuestionMarkIcon />,
    text: 'Text',
    arrow: true

  },
  {
    id:10,
    icon: <PriorityHighIcon />,
    text: 'Send feedback',
    arrow: true
  }
];